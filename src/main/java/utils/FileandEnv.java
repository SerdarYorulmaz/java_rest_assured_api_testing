package utils;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class FileandEnv {

    /**
     * Hangi ortamda dev /qa / staging
     */

    public static Map<String, String> fileandenv = new HashMap<String, String>();
    public static Properties propMain = new Properties();
    public static Properties propPreSet = new Properties();

    public static Map<String, String> envAndFile(String temp) {

        String enviroment = System.getProperty("env");
        enviroment=temp;

        try {
            if (enviroment.equalsIgnoreCase("dev")) {
                FileInputStream fisDev = new FileInputStream(System.getProperty("user.dir") + "/inputs/dev.properties");
                propMain.load(fisDev);
                fileandenv.put("ServerUrl", propMain.getProperty("ServerUrl"));
                fileandenv.put("portNo", propMain.getProperty("portNo"));
                fileandenv.put("apiKey", propMain.getProperty("423f2562"));


            } else if (enviroment.equalsIgnoreCase("qa")) {


                FileInputStream fisQA = new FileInputStream(System.getProperty("user.dir") + "/inputs/qa.properties");
                propMain.load(fisQA);
                fileandenv.put("ServerUrl", propMain.getProperty("ServerUrl"));
                fileandenv.put("portNo", propMain.getProperty("portNo"));
                fileandenv.put("apiKey", propMain.getProperty("423f2562"));


            }
        } catch (Exception e) {

        }

        return fileandenv;
    }

    public static Map<String, String> getConfigReader(String temp) {
        if (fileandenv == null) {
            fileandenv = envAndFile(temp);
        }
        return fileandenv;
    }

}
