package baseTest;

import com.relevantcodes.extentreports.LogStatus;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentReportListner;
import utils.FileandEnv;

@Listeners(ExtentReportListner.class)
public class BaseTest extends ExtentReportListner {

    @BeforeClass
    public String baseTest(){
       return RestAssured.baseURI=FileandEnv.envAndFile("dev").get("ServerUrl");
    }
}
